
// Require modules
const express    = require('express');
const bodyParser = require('body-parser');
const connection = require('./config/db');
const path       = require('path');
const ejs        = require("ejs");
const app        = express();
const multer     = require('multer');
const cookieParser = require('cookie-parser');
const jwt = require('jsonwebtoken');

var accountSid = 'AC2ed93b4650e387a34b2d91ec42b45dda';
var authToken = '00d471248a6e5bebb7bb565309b0c699';
const client = require('twilio')(accountSid, authToken);

var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
app.use(cookieParser());
app.use(require('cookie-parser')());
app.use(require('express-session')({ secret: 'keyboard cat', resave: true, saveUninitialized: true }));


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:true}));

app.use(express.static(path.join(__dirname, '/public')));
app.use(express.static(path.join(__dirname, '/views')));

// uploaded file 
let storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, 'public/uploads')
    },
    filename: (req, file, cb) => {
        cb(null,Date.now() +'-'+file.originalname)
    }
});
let upload = multer({storage: storage});

// Register handlebars with the express app.
app.set('view engine', 'ejs');

// Set port for server
var port = process.env.PORT || 8080;
// Make server listen to port
app.listen(port,()=>{
    console.log("server is running on port : " + port);
});



// Set route for register page
app.get('/register',(req,res)=>{

    const token = req.cookies.token;

    jwt.verify(token,'mySeCrEtKeY1987',(err,data)=>{
        if(err) {
            res.render("pages/register.ejs",{
            });
        } else {
            res.redirect("/main")
        }
    })
 });

// Set route for register page
app.post('/register',upload.single('agentImage'),(req,res)=>{

    let agent_type = req.body.agentType;
    let agent_name = req.body.agentName;
    let agent_phone = req.body.agentPhone;
    let agent_email = req.body.agentEmail;
    let agent_password = req.body.agentPassword;
    let agent_address = req.body.agentAddress;
    let agent_image ;
    if(req.file){
        agent_image = req.file.filename;
    } else {
        agent_image = 'default.jpg';
    }
    let agent_description = req.body.agentDescription;

    connection.query('CALL RegisterAgent(?,?,?,?,?,?,?,?)',
    [agent_type,agent_name,agent_phone,agent_email,agent_password,agent_address,agent_image,agent_description],
    (error,result)=>{
        res.redirect("/")
        });
        
 });

 // Set route for login page
 app.get('/',(req,res)=>{

    const token = req.cookies.token;

    jwt.verify(token,'mySeCrEtKeY1987',(err,data)=>{
        if(err) {
            res.render("pages/login.ejs",{
            });
        } else {
            res.redirect("/main")
        }
    })
 });

// Set route for login page
app.post('/login',(req,res)=>{

    let email = req.body.agentEmail;
    let password = req.body.agentPassword;


    connection.query("CALL Login(?,?);",[email,password],(error,result)=>{

        if(result.length > 0) {

            const data = {
                agent_uuid :result[0][0].uuid,
                agentName:result[0][0].agent_name,
                agentImage :result[0][0].agent_image_path
            };

            jwt.sign(data,'mySeCrEtKeY1987',{expiresIn: '14d' },(err,token)=>{

                if(err){
                    console.log(err);
                }else {
                    res.cookie('token',token,{ maxAge:14 * 24 * 3600000, httpOnly:true });
                    res.redirect("/main")
                }
            });

        } else {
            res.render('pages/login')
        }
   });
});
app.get('/logout',(req,res)=>{
    res.cookie('token',{ maxAge:1, httpOnly:true });
    res.redirect("/")
});

// Set route for dashboard
app.get('/main', (req,res)=>{

    const token = req.cookies.token;

    jwt.verify(token,'mySeCrEtKeY1987',(err,data)=>{
        if(err) {
            res.sendStatus(403);
        } else {
            // agent.agent_uuid
            connection.query('CALL GetDashboardInfo(?);',[data.agent_uuid],(error,results)=>{

                res.render("index",{
                    agentName:data.agentName,
                    agentImage:data.agentImage,
                    customerCount: results[0][0].customerCount,
                    deliverd: results[1][0].deliverd,
                    suspended: results[2][0].suspended,
                    sales: results[3][0].sales,
                    orders: results[4],
                    isActive: "active"
                });
            });

        }
    })
});

// Set route for get all items of specific agent
app.get('/items',(req,res)=>{

    const token = req.cookies.token;

    jwt.verify(token,'mySeCrEtKeY1987',(err,data)=>{
        if(err) {
            res.sendStatus(403);
        } else {
            connection.query('CALL GetAllItems(?)',[data.agent_uuid],(error,results)=>{
                res.render('pages/items.ejs',{
                    items: results[0],
                    itemsCount:results[1][0].itemsCount,
                    itemsCost:results[2][0].itemsCost,
                    agentName:data.agentName,
                    agentImage:data.agentImage,
                    isActive: "active"
                });
            });
        }

    });
});


// Set route for add item in database
app.post('/items/insert',upload.single('addItemImage'),(req,res)=>{

    const token = req.cookies.token;

    jwt.verify(token,'mySeCrEtKeY1987',(err,data)=>{
        if(err) {
            res.sendStatus(403);
        } else {
            let agent_id = data.agent_uuid;
            let category_id = req.body.addItemCategory;
            let item_name = req.body.addItemName;
            let item_price = req.body.addItemPrice;
            let item_description = req.body.addItemDescription;
            let item_image;
            if(req.file){
                 item_image = req.file.filename;
            } else {
                 item_image = 'default.jpg';
            }

            connection.query('CALL InsertItem(?,?,?,?,?,?)',
            [agent_id,item_name,item_price,item_description,item_image,category_id],(error,results)=>{
                res.redirect("/items");
            });
        }

    });

});

// Set route for edit item
app.get('/items/:item_id',(req,res)=>{

    const token = req.cookies.token;

    jwt.verify(token,'mySeCrEtKeY1987',(err,data)=>{
        if(err) {
            res.sendStatus(403);
        } else {
            let item_id = req.params.item_id;
            let agent_id = data.agent_uuid;
            connection.query('CALL GetItem(?,?)',[item_id,agent_id],(error,result)=>{
                res.send(result);
            });
        }
    });
});

// Set route for update item
app.post('/items/update',upload.single('itemImage'),(req,res)=>{

        const token = req.cookies.token;

        jwt.verify(token,'mySeCrEtKeY1987',(err,data)=>{
            let item_id = req.body.itemId;
            let category_id = req.body.itemCategory;
            let item_name = req.body.itemName;
            let item_price = req.body.itemPrice;
            let item_description = req.body.itemDescription;
            let item_image ;
            if(!req.file){
                item_image = req.body.itemImageEdit;
            } else {
                item_image = req.file.filename;
            }

            connection.query('CALL UpdateItem(?,?,?,?,?,?)',
            [item_id,item_name,item_price,item_description,item_image,category_id],(error,result)=>{
                res.redirect("/items");
                });
        });

});

// Set route for delete item
app.get('/items/delete/:item_id',(req,res)=>{

    let item_id = req.params.item_id;

    const token = req.cookies.token;

    jwt.verify(token,'mySeCrEtKeY1987',(err,data)=>{
        connection.query('CALL DeleteItem(?)',[item_id],(error,result)=>{
            res.redirect("/items");
        });
    });

});

// Set route for get all order of specific agent
app.get('/orders/',(req,res)=>{
    
    const token = req.cookies.token;

    jwt.verify(token,'mySeCrEtKeY1987',(err,data)=>{
        if(err) {
            res.sendStatus(403);
        } else {
            let agent_id = data.agent_uuid;
            connection.query('CALL GetAllOrders(?)',[agent_id],(error,results)=>{
            res.render('pages/orders.ejs',{
            agentName:data.agentName,
            agentImage:data.agentImage,
            deliverd: results[0][0].deliverd,
            underDelivery: results[1][0].underDelivery,
            reject: results[2][0].reject,
            suspended: results[3][0].suspended,
            ordersCount:results[5][0].ordersCount,
            ordersCost:results[6][0].ordersCost,
            orders: results[4],
            isActive: "active"
        });
    });
        }
    })

});

// Set route for show order details
app.get('/orders/details/:order_id',(req,res)=>{

    const token = req.cookies.token;

    jwt.verify(token,'mySeCrEtKeY1987',(err,data)=>{
        if(err) {
            res.sendStatus(403);
        } else {
            let order_id = req.params.order_id
            connection.query('CALL GetOrderDetails(?)',[order_id],(error,results)=>{
                res.send(results);
            });
        }
    })

});

// Set route for accept order 
app.get('/orders/accept/:order_id/:page',(req,res)=>{

    const token = req.cookies.token;

    jwt.verify(token,'mySeCrEtKeY1987',(err,data)=>{
        if(err) {
            res.sendStatus(403);
        } else {
            let order_id = req.params.order_id;
            let page = req.params.page;
            let order_type = 1 ;
            let time = new Date();
            connection.query('CALL ChangeOrderStutes(?,?,?)',[order_id,order_type,time],(error,results)=>{
                if(page == 'orders'){
                    res.redirect("/orders");
                }else{
                    res.redirect("/main"); 
                }
            })
        }
        
    });

});

// Set route for reject order 
app.get('/orders/reject/:order_id/:page',(req,res)=>{
 

    const token = req.cookies.token;

    jwt.verify(token,'mySeCrEtKeY1987',(err,data)=>{
        if(err) {
            res.sendStatus(403);
        } else {
            let order_id = req.params.order_id;
            let page = req.params.page;
            let order_type = 0;
            let time = new Date();
            connection.query('CALL ChangeOrderStutes(?,?,?)',[order_id,order_type,time],(error,results)=>{
                if(page == 'orders'){
                    res.redirect("/orders");
                }else{
                    res.redirect("/main"); 
                }
            });
        }
        
    });

});

// Set route for get all categories of specific agent
app.get('/categories',(req,res)=>{

    const token = req.cookies.token;

    jwt.verify(token,'mySeCrEtKeY1987',(err,data)=>{
        if(err) {
            res.sendStatus(403);
        } else {
                let agent_id = data.agent_uuid;
                connection.query('CALL GetAllCategories(?)',[agent_id],(error,results)=>{
                res.render('pages/categories.ejs',{
                categories: results[0],
                agentName:data.agentName,
                agentImage:data.agentImage,
                categoriesCount:results[1][0].categoriesCount,
                isActiveCategories: "active"
             });
          });
        }
    })

});

// Set route for add category 
app.get('/categories/get',(req,res)=>{

    const token = req.cookies.token;

    jwt.verify(token,'mySeCrEtKeY1987',(err,data)=>{
        if(err) {
            res.sendStatus(403);
        } else {
            let agent_id = data.agent_uuid;
            connection.query('CALL GetAllCategories(?)',[agent_id],(error,results)=>{
                res.send(results[0]);
            })
        }
    })

});

// Set route for insert gategory in database
app.post('/categories/add',upload.single('addCategoryImage'),(req,res)=>{

    const token = req.cookies.token;

    jwt.verify(token,'mySeCrEtKeY1987',(err,data)=>{
        if(err) {
            res.sendStatus(403);
        } else {
            let agent_id = data.agent_uuid;
            let category_name = req.body.addCategoryName;
            let category_image;
            if(req.file){
                category_image = req.file.filename;
           } else {
                category_image = 'default.jpg';
           }
            

            connection.query('CALL InsertCategory(?,?,?)',
                [agent_id,category_name,category_image],(error,results)=>{
                    res.redirect("/categories");
            });
        }
    })

});

// Set route for edit category
app.get('/categories/:category_id',(req,res)=>{

    const token = req.cookies.token;

    jwt.verify(token,'mySeCrEtKeY1987',(err,data)=>{
        if(err) {
            res.sendStatus(403);
        } else {
            let category_id = req.params.category_id;
            connection.query('CALL GetCateory(?)',[category_id],(error,result)=>{
                res.send(result[0]);
            });
        }
    })

});

// Set route for update item
app.post('/categories/update',upload.single('editCategoryImage'),(req,res)=>{

    const token = req.cookies.token;

    jwt.verify(token,'mySeCrEtKeY1987',(err,data)=>{
        if(err) {
            res.sendStatus(403);
        } else {
            let category_id = req.body.editCategoryId;
            let category_name = req.body.editCategoryName;
            let category_image ;
            if(!req.file){
                category_image = req.body.categoryImage;
            } else {
                category_image = req.file.filename;
            }

            connection.query('CALL UpdateCategory(?,?,?)',
            [category_id,category_name,category_image],(error,result)=>{
                res.redirect("/categories");
            });
        }
    })

});

// Set route for delete category
app.get('/categories/delete/:category_id',(req,res)=>{

    const token = req.cookies.token;

    jwt.verify(token,'mySeCrEtKeY1987',(err,data)=>{
        if(err) {
            res.sendStatus(403);
        } else {
            let category_id = req.params.category_id;

            connection.query('CALL DeleteCategory(?)',[category_id],(error,result)=>{
                res.redirect("/categories");
            });
        }
    })

});


// Verify Token
function verifyToken(req, res, next) {
    // Get auth header value
    const bearerHeader = req.headers['authorization'];
    // Check if bearer is undefined
    if(typeof bearerHeader !== 'undefined') {
        // Split at the space
        const bearer = bearerHeader.split(' ');
        // Get token from array
        const bearerToken = bearer[1];
        // Set the token
        req.token = bearerToken;
        // Next middleware
        next();
    } else {
        // Forbidden
        res.sendStatus(403);
    }

}

/////////////////////////////Mobile Api////////////////////////////////////////////

//login
app.post('/api/getConfigCode',(req,res)=>{
    let PhoneNo = req.body.PhoneNo;
    PhoneNo = '+964' + PhoneNo;
    console.log(PhoneNo);
    let resMsg = "";
    let randCode = Math.floor(1000 + Math.random() * 9000);

    client.messages.create(
        {
            to: PhoneNo.toString(),
            from: '+1 727 513 2949',
            body: 'مرحبا بك كود التفعيل هو '+randCode+' شكرا لك لاستخدام أكلاتي',
        },
        (err, message) => {
            if(err) {
                console.log(err);
                resMsg = {"Code":'err'};
                res.json({"EnterCode":resMsg});
            }
            else{
                console.log('sending message');
                resMsg = {"Code":randCode};
                res.json({"EnterCode":resMsg});
            }
        });
});

app.post('/api/checkPhoneHaveAccount',(req,res)=>{

    let PhoneNo = req.body.PhoneNo;
    connection.query('CALL Mob_checkPhoneHaveAccount(?);',[PhoneNo],(err,result)=>{
        if(err){
            res.status(404).json(err);
        }else {
            res.json({"result":result[0]});
        }
    });
});

app.post('/api/insertUserGetUUID',(req,res)=>{

    let PhoneNo = req.body.PhoneNo;
    let Name = req.body.Name;
    connection.query('CALL Mob_insertUserGetUUID(?,?);',[Name,PhoneNo],(err,result)=>{
        if(err){
            res.status(404).json(err);
        }else {
            res.json({"user_uuid":result[0]});
        }
    });
});
//end of login

// api get more Agent by max rate and type
app.post('/api/MoreAgent',(req,res)=>{

    let fromId       = req.body.fromId;
    let agent_type   = req.body.agent_type;

    connection.query('CALL Mob_AgentGetMoreByType(?,?);',[fromId,agent_type],(err,result)=>{
        if(err){
            res.status(404).json(err);
        }else {
            res.json({"moreAgent":result[0]});
        }
    });
});
// api get more foods by max rate (restruent and home)

app.post('/api/MoreFoods',(req,res)=>{

    let fromId = req.body.fromId;

    connection.query('CALL Mob_FoodGetMore(?);',[fromId],(err,result)=>{
        if(err){
            res.status(404).json(err);
        }else {
            res.json({"moreFoods":result[0]});
        }
    })
});
// api get Info of specific food and all foods of this agent

app.post('/api/FoodInfo',(req,res)=>{

    let food_id = req.body.food_id;

    connection.query('CALL Mob_FoodsGetInfo(?);',[food_id],(err,result)=>{
        if(err){
            res.status(404).json(err);
        }else {
            res.json({"foodRestInfo":result[0],"foodsInfo":result[1]});
        }
    });
});

// api get search result from food and agent
app.post('/api/Search',(req,res)=>{

    let search_term = req.body.search_term;

    connection.query('CALL Mob_Search(?);',[search_term],(err,result)=>{
        if(err){
            res.status(404).json(err);
        }else {
            res.json({"search":result[0]});
        }
    });
});

// get the top items resturant house made
app.get('/Api/getTopItemByRate',(req,res)=>{
    connection.query('CALL Mob_getTopItemByRate();',(err,result)=>{
        if(err)
        {
            res.status(404).json(err)
        }
        else {
            {
                res.status(200).json({"bestFood":result[0] ,"bestResturan":result[1] ,"bestHouse":result[2]})
            }
        }
    });
});

// api get agent foods
app.post('/api/AgentFoods',(req,res)=>{

    let agent_id = req.body.agent_id;

    connection.query('CALL Mob_GetAgentItems(?);',[agent_id],(err,result)=>{
        if(err){
            res.status(404).json(err);
        }else {
            res.json({"agentFoods":result[0]});
        }
    });
});




// upload Bill
app.post('/api/uploadBill',(req,res)=>{

    let jsonOrder = JSON.parse(req.body.jsonOrder);
    let arrItems = JSON.parse(req.body.arrItems);

    // console.log(arrItems);
   //  console.log(jsonOrder);
    let PaymentMethod = jsonOrder.PaymentMethod;
    let PaymentType;
    if(PaymentMethod == 'على الراتب'){
        PaymentType = 2;
    }else if(PaymentMethod == 'اقساطي'){
        PaymentType = 3;
    }else if(PaymentMethod == 'نقدي'){
        PaymentType = 1;
    }else if(PaymentMethod == 'كي كارد'){
        PaymentType = 4;
    }
    connection.query('CALL Mob_InsertOrder(?,?,?,?,?,?,?);',[
        jsonOrder.customerUuid,jsonOrder.agentUuid,jsonOrder.address,
        jsonOrder.subTotalCost,jsonOrder.disCount,jsonOrder.FinalCost,PaymentType],
        (err,result)=>{
        if(err){
            res.status(404).json({"upload":"err"});
        }else {
            let OrderUuid = result[0][0].uuid;
            arrItems.map(function(entry) {
                entry.OrderUuid = OrderUuid;
            });

            let strArrObj = '';
            for(var item of arrItems) {
                let itemUuid = '\''+item.itemUuid+'\'';
                let itemQnt = '\''+item.itemQnt+'\'';
                let itemSubTotal = '\''+item.itemSubTotal+'\'';
                let itemNote = '\''+item.itemNote+'\'';
                let OrderUuid = '\''+item.OrderUuid+'\'';
                strArrObj += '('
                +itemUuid+','
                +itemQnt +','
                +itemSubTotal+','
                +itemNote+','
                +OrderUuid+'),';
            }
            strArrObj = strArrObj.substring(0, strArrObj.length-1);

            var sql = "INSERT INTO order_details (items_meal_uuid,quantity,subtotal,items_meal_notes,order_uuid) VALUES "+strArrObj+"";
           
            connection.query(sql, function(err) {
                 if (err) res.json({"upload":"err"});
                 else
                 res.json({"upload":"successUpload"});
             });
        }
    });
  
  });


